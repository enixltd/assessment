# Note API Technical Assessment

Laravel Note API Technical Assessment 

Build a note API using Laravel where a user has many notes. User endpoints should demonstrate the number of notes a user has created and be ordered by the total number of likes through notes.

#### The api should have the following features:

Users:
A user has a username and password.
```
- A user can create an account
- A user can delete their account
- A user can update their username
- A user can login
- A user can list all users
- A user can filter users
```

Notes:
A note has a name and a description and belongs to many notes.
```
- A user can create, update and delete a note
- A user can list their notes
- A user can provide a search term to filter their notes
- A user can like a note that is not their own
- A user can list all notes
- A user can filter all notes
```

Authentication can be implemented via standard token, Bearer token via JWT or OAuth.

Each endpoint should have integration / feature tests & and services should be unit tested. This does not need to be complete but will help demonstrate how the candidate develops services, any questions regarding the assessment are welcome. The assessment should be committed to GitHub.

