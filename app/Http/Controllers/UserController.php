<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\IndexRequest;
use App\Http\Resources\User\UserResourceCollection;
use App\Services\User\Index;

class UserController extends Controller
{
    /**
     * List users.
     *
     * @param Index $index
     * @param IndexRequest $request
     * @return UserResourceCollection
     */
    public function index(Index $index, IndexRequest $request)
    {
        $page = $request->input('page');
        $term = $request->input('term');

        $users = $index($page, $term);

        return new UserResourceCollection($users);
    }
}
