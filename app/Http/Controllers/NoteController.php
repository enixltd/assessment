<?php

namespace App\Http\Controllers;

use App\Http\Requests\Note\IndexRequest;
use App\Http\Requests\Note\StoreRequest;
use App\Http\Resources\Note\NoteResource;
use App\Http\Resources\Note\NoteResourceCollection;
use App\Models\Note;
use App\Services\Note\Index;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class NoteController extends Controller
{
    /**
     * Create a note.
     *
     * @param StoreRequest $request
     * @return JsonResponse
     */
    public function store(StoreRequest $request)
    {
        $validated = $request->validated();
        $validated['user_id'] = 1; // TODO: Where 1 is auth()->id()

        $note = Note::create($validated);

        $message = __('service.note.store');

        return (new NoteResource($note))
            ->additional(compact('message'))
            ->response()
            ->setStatusCode(201);
    }

    /**
     * List notes.
     *
     * @param Index $index
     * @param IndexRequest $request
     * @return NoteResourceCollection
     */
    public function index(Index $index, IndexRequest $request)
    {
        $page = $request->input('page');
        $term = $request->input('term');
        $isUser = $request->input('is_user', false);

        $notes = $index($page, $isUser, $term);

        return new NoteResourceCollection($notes);
    }

    /**
     * Update a note.
     *
     * @param Note $note
     * @param StoreRequest $request
     * @return Response
     */
    public function update(Note $note, StoreRequest $request)
    {
        $validated = $request->validated();

        $note->update($validated);

        $message = __('service.note.update');

        return response(compact('message'));
    }

    /**
     * Delete a note.
     *
     * @param Note $note
     * @return Response
     * @throws Exception
     */
    public function destroy(Note $note)
    {
        $note->delete();

        $message = __('service.note.destroy');

        return response(compact('message'));
    }

    /**
     * Toggle the like state of a note.
     *
     * @param Note $note
     * @return Response
     */
    public function like(Note $note)
    {
        $user = 1; // TODO: Where 1 is auth()->id()

        $isLiked = $note->users->contains($user);

        if ($user->id !== $note->user_id) {
            $note->users()->attach($user, ['is_liked' => !$isLiked]);
        }

        $state = !$isLiked ? 'liked' : 'disliked';

        $message = __("service.note.{$state}");

        return response(compact('message'));
    }
}
