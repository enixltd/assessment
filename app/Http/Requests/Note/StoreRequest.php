<?php

namespace App\Http\Requests\Note;

use App\Rules\AlphaSpace;
use App\Rules\Ascii;
use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'max:25', new AlphaSpace()],
            'description' => ['required', 'max:800', new Ascii()],
        ];
    }
}
