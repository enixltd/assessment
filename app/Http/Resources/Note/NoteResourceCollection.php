<?php

namespace App\Http\Resources\Note;

use Illuminate\Http\Resources\Json\ResourceCollection;

class NoteResourceCollection extends ResourceCollection
{
    /**
     * The resource name to wrap.
     *
     * @var string
     */
    public static $wrap = 'notes';
}
