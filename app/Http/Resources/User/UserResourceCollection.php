<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\ResourceCollection;

class UserResourceCollection extends ResourceCollection
{
    /**
     * The resource name to wrap.
     *
     * @var string
     */
    public static $wrap = 'users';
}
