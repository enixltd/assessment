<?php

namespace App\Services\Note;

use App\Models\Note;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class Index
{
    /**
     * List notes.
     *
     * @param int $page
     * @param bool $isUser
     * @param string|null $term
     * @return LengthAwarePaginator
     */
    public function __invoke(int $page, bool $isUser, ?string $term = null)
    {
        $query = Note::query();

        if (!is_null($query)) {
            $query->where('name', 'LIKE', "%$term%");
        }

        if ($isUser) {
            $query->where('user_id', 1); // TODO: Where 1 is auth()->id()
        }

        return $query->paginate(10, ['id', 'name', 'description'], 'page', $page);
    }
}
