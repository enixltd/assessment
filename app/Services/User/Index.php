<?php

namespace App\Services\User;

use App\Models\User;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class Index
{
    /**
     * List users.
     *
     * @param int $page
     * @param string|null $term
     * @return LengthAwarePaginator
     */
    public function __invoke(int $page, ?string $term = null)
    {
        $query = User::query();

        if (!is_null($query)) {
            $query->where('username', 'LIKE', "%$term%");
        }

        return $query->paginate(10, ['id', 'username'], 'page', $page);
    }
}
