<?php

namespace Tests\Unit\User;

use App\Services\User\Index;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Tests\TestCase;

class IndexTest extends TestCase
{
    public function test_it_lists_users()
    {
        $this->assertInstanceOf(
            LengthAwarePaginator::class,
            app(Index::class)(1, false)
        );
    }
}
