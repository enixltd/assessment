<?php

namespace Tests\Unit\Note;

use App\Services\Note\Index;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Tests\TestCase;

class IndexTest extends TestCase
{
    public function test_it_lists_notes()
    {
        $this->assertInstanceOf(
            LengthAwarePaginator::class,
            app(Index::class)(1, false)
        );
    }
}
