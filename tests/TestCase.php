<?php

namespace Tests;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    /**
     * @trait CreatesApplication
     */
    use CreatesApplication;

    /**
     * @trait RefreshDatabase
     */
    use RefreshDatabase;
}
