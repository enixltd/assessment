<?php

namespace Tests\Feature\User;

use Tests\TestCase;

class IndexTest extends TestCase
{
    public function test_it_lists_users()
    {
        $this->json('GET', '/users')
            ->assertJsonStructure(['message'])
            ->assertStatus(200);
    }
}
