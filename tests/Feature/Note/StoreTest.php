<?php

namespace Tests\Feature\Note;

use Tests\TestCase;

class StoreTest extends TestCase
{
    /**
     * Define note attributes.
     *
     * @return string[]
     */
    protected function params(): array
    {
        return [
            'name' => 'foo',
            'description' => 'bar',
        ];
    }

    public function test_it_creates_a_note()
    {
        $this->json('POST', '/notes', $this->params())
            ->assertJsonStructure(['message'])
            ->assertStatus(200);
    }
}
