<?php

namespace Tests\Feature\Note;

use App\Models\Note;
use App\Models\User;
use Tests\TestCase;

class UpdateTest extends TestCase
{
    /**
     * @var Note
     */
    protected $note;

    /**
     * Setup the test case.
     */
    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create();

        $this->note = Note::factory(['user_id' => $user->id])->create();
    }

    /**
     * Define note attributes.
     *
     * @return string[]
     */
    protected function params(): array
    {
        return [
            'name' => 'Foo',
        ];
    }

    public function test_it_updates_a_note()
    {
        $this->json('PUT', "/notes/{$this->note->id}", $this->params())
            ->assertJsonStructure(['message'])
            ->assertStatus(200);
    }
}
