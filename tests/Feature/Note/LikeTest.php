<?php

namespace Tests\Feature\Note;

use App\Models\Note;
use App\Models\User;
use Tests\TestCase;

class LikeTest extends TestCase
{
    public function test_it_likes_a_note()
    {
        $owner = User::factory()->create();

        $note = Note::factory(['user_id' => $owner->id]);

        $this->json('POST', "/notes/{$note}/like")
            ->assertJsonStructure(['message'])
            ->assertStatus(200);
    }
}
