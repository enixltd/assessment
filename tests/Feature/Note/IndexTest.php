<?php

namespace Tests\Feature\Note;

use Tests\TestCase;

class IndexTest extends TestCase
{
    public function test_it_lists_notes()
    {
        $this->json('GET', '/notes', ['page' => 1])
            ->assertJsonStructure(['message'])
            ->assertStatus(200);
    }
}
