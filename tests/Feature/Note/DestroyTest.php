<?php

namespace Tests\Feature\Note;

use App\Models\Note;
use App\Models\User;
use Tests\TestCase;

class DestroyTest extends TestCase
{
    public function test_it_deletes_a_note()
    {
        $user = User::factory()->create();

        $note = Note::factory(['user_id' => $user->id])->create();

        $this->json('DELETE', "/notes/{$note->id}")
            ->assertJsonStructure(['message'])
            ->assertStatus(200);
    }
}
