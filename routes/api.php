<?php

use App\Http\Controllers\NoteController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Users.

Route::get('/users', [UserController::class, 'index']);

// Notes.

Route::pattern('note', '[0-9]+');

Route::post('/notes', [NoteController::class, 'store']);

Route::get('/notes', [NoteController::class, 'index']);

Route::put('/notes/{note}', [NoteController::class, 'update']);

Route::post('/notes/{note}/like', [NoteController::class, 'like']);
